﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Ball_Reset : MonoBehaviour
{
    // Inspector variables
    [SerializeField] private Vector2 ballStartPosition;
    [SerializeField] private float moveToPositionSpeed = 0.1f;
    [SerializeField] private float delayToReset = 1f;
    [SerializeField] private float startDelay = 1f;
    [SerializeField] private Rigidbody2D ballRB;

    // Private variables
    public static int ballBounceLeft = 0;
    public static int ballBounceRight = 0;

    // Init
    private void Start()
    { 
        StartCoroutine(ResetBall());
    }

    // Main
    private void Update()
    {
        if (ballRB.position.y < -10f)
            if (!Logic.isReseting)
            {
                StartCoroutine(ResetBall());
            }
    }

    // Reset game on ball collision
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (Logic.lastHitPlayerID > -1)
        {
            if (!Logic.isReseting)
            {
                if (collision.collider.name == "Net")
                    StartCoroutine(ResetBall());
                else
                {
                    // Ball allowed to bounce once
                    if (ballRB.position.x < 0f)
                    {
                        ballBounceLeft++;
                        ballBounceRight = 0;
                        if (Logic.lastHitPlayerID == 2)
                        {
                            if (ballBounceLeft >= 2) StartCoroutine(ResetBall());
                        }
                        else if (Logic.lastHitPlayerID == 1) 
                            StartCoroutine(ResetBall());
                    }
                    else
                    {
                        ballBounceRight++;
                        ballBounceLeft = 0;
                        if (Logic.lastHitPlayerID == 1)
                        {
                            if (ballBounceRight >= 2) StartCoroutine(ResetBall());
                        }
                        else if (Logic.lastHitPlayerID == 2)
                            StartCoroutine(ResetBall());
                    }
                }
            }
        }
    }

    // Reset ball position
    IEnumerator ResetBall()
    {
        Logic.isReseting = true;
        ballRB.velocity = Vector2.zero;
        ballRB.angularVelocity = 0f;
        ballRB.isKinematic = true;
        yield return new WaitForSeconds(delayToReset);
        while (Vector2.Distance(ballRB.position, ballStartPosition) > 0.1f)
        {
            if (!Logic.isPaused)
                ballRB.position = Vector2.Lerp(ballRB.position, ballStartPosition, moveToPositionSpeed * Time.deltaTime);
            yield return null;
        }
        yield return new WaitForSeconds(startDelay);
        ballRB.isKinematic = false;
        Logic.isReseting = false;
        Logic.lastHitPlayerID = -1;
        ballBounceLeft = 0;
        ballBounceRight = 0;
    }
}
