﻿using UnityEngine;
using System.Collections;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class Flicker : MonoBehaviour
{
    // Inspector variables
    [SerializeField] private float minBloomIntensity;
    [SerializeField] private float maxBloomIntensity;
    [SerializeField] private float bloomFlickerDelay;

    // Private variables
    private Volume volume;
    private Bloom bloomLayer;

    // Init
    void Start()
    {
        volume = gameObject.GetComponent<Volume>();
        volume.profile.TryGet<Bloom>(out bloomLayer);
        StartCoroutine(UpdateBloom());
    }

    // Bloom flicker
    IEnumerator UpdateBloom()
    {
        while (true)
        {
            bloomLayer.intensity.value = Random.Range(minBloomIntensity, maxBloomIntensity);
            yield return new WaitForSeconds(bloomFlickerDelay);
        }
    }
}
