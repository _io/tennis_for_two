﻿using UnityEngine;

public class Knob : MonoBehaviour
{
    // Inspector variables
    [SerializeField] private Joystick joystick;

    // Main loop
    private void Update()
    {
        if (joystick.Horizontal != 0f || joystick.Vertical != 0f)
        {
            float angle = Vector2.SignedAngle(Vector2.up, new Vector2(joystick.Horizontal, joystick.Vertical));
            gameObject.transform.rotation = Quaternion.Euler(0f, 0f, angle);
        }
    }
}
