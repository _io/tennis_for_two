﻿using UnityEngine;
using UnityEngine.UI;

public class Logic : MonoBehaviour
{
    // Inspector variables
    [SerializeField] private float hitSpeed = 12f;
    [SerializeField] private float aiHitAngleOffset = 270f;
    [SerializeField] private GameObject ballObject;
    [SerializeField] private Rigidbody2D ballRB;
    [SerializeField] private AudioSource audioSource;
    [SerializeField] private AudioClip[] hitSounds;
    [SerializeField] private GameObject knob;
    [SerializeField] private GameObject knobBackground;
    [SerializeField] private GameObject hitButton;
    [SerializeField] private float knobRotationOffset = 90f;
    [SerializeField] private Text aiToggleText;
    [SerializeField] private Text audioToggleText;
    [SerializeField] private Text controlsToggleText;
    [SerializeField] private bool disableTouchControls;
    [SerializeField] private GameObject controlsToggle;

    // Static variables
    public static bool isPaused = false;
    public static bool isReseting = false;
    public static int lastHitPlayerID = -1;

    // Private variables
    private bool isAIEnabled = true;
    private bool isAudioEnabled = true;
    private bool areControlsEnabled = true;
    private Vector2 ballVelocity;

    // Init
    private void Start()
    {
        if (disableTouchControls)
        {
            controlsToggle.SetActive(false);
            if (areControlsEnabled)
                ToggleControls();
        }
    }

    // Main loop
    void Update()
    {
        if (ballObject.activeSelf == true)
        {
            // Player touch controls
            if (!areControlsEnabled && !isReseting)
            {
                bool inputDetected = false;
                Vector2 touchPosition = new Vector2();

                // Mouse input
                if (Input.GetMouseButtonDown(0))
                {
                    inputDetected = true;
                    touchPosition = Input.mousePosition;
                }

                // Touch input
                if (Input.touchCount > 0)
                {
                    inputDetected = true;
                    Touch touch = Input.GetTouch(0);
                    if (touch.phase == TouchPhase.Began)
                        touchPosition = touch.position;
                }

                if (inputDetected)
                {
                    // Calculate hit force
                    Vector3 ballPos = Camera.main.WorldToScreenPoint(new Vector3(ballRB.position.x, ballRB.position.y, 0));
                    Vector2 force = (new Vector2(ballPos.x, ballPos.y) - touchPosition).normalized * hitSpeed;

                    // Only hit ball on certain conditions
                    if ((ballRB.transform.position.x < 0f && lastHitPlayerID != 1) || (!isAIEnabled && ballRB.transform.position.x > 0f && lastHitPlayerID != 2))
                    {
                        // Hit ball
                        Ball_Reset.ballBounceRight = 0;
                        ballRB.velocity = force;
                        audioSource.pitch = Random.Range(0.95f, 1.05f);
                        audioSource.clip = hitSounds[Random.Range(0, hitSounds.Length - 1)];
                        audioSource.Play();
                        if (ballRB.transform.position.x < 0f)
                            lastHitPlayerID = 1;
                        else
                            lastHitPlayerID = 2;
                    }
                }
            }

            // AI
            if (isAIEnabled && !isReseting)
            {
                float hitRange = Random.Range(0f, 3f);
                if ((ballRB.position.x > 0f + hitRange) && lastHitPlayerID != 2)
                {
                    int hitChance = Random.Range(1, 100);

                    // Hit ball
                    if (hitChance > 90)
                    {
                        Ball_Reset.ballBounceLeft = 0;
                        float angle = Mathf.LerpAngle(60f, 120f, (ballRB.position.y)) - aiHitAngleOffset;
                        float xForce = Mathf.Cos(angle * Mathf.Deg2Rad);
                        float yForce = Mathf.Sin(angle * Mathf.Deg2Rad);
                        Vector2 force = new Vector2(xForce, yForce).normalized * hitSpeed;
                        ballRB.velocity = force;
                        audioSource.pitch = Random.Range(0.95f, 1.05f);
                        audioSource.clip = hitSounds[Random.Range(0, hitSounds.Length - 1)];
                        audioSource.Play();
                        lastHitPlayerID = 2;
                    }
                }
            }
        }
    }

    // Called by Hit button
    public void HitButton()
    {
        if (!isPaused && !isReseting)
        {
            if ((ballRB.transform.position.x < 0f && lastHitPlayerID != 1) || (!isAIEnabled && ballRB.transform.position.x > 0f && lastHitPlayerID != 2))
            {
                float angle = knob.transform.rotation.eulerAngles.z + knobRotationOffset;
                Vector2 direction = new Vector2(Mathf.Cos(angle * Mathf.Deg2Rad), Mathf.Sin(angle * Mathf.Deg2Rad));
                ballRB.velocity = direction * hitSpeed;
                audioSource.clip = hitSounds[Random.Range(0, hitSounds.Length - 1)];
                audioSource.pitch = Random.Range(0.95f, 1.05f);
                audioSource.Play();
                if (ballRB.transform.position.x < 0f)
                    lastHitPlayerID = 1;
                else
                    lastHitPlayerID = 2;
            }
        }
    }

    // Called by Pause button
    public void PauseGame()
    { 
        isPaused = true;
        ballVelocity = ballRB.velocity;
        ballRB.velocity = Vector2.zero;
        ballRB.isKinematic = true;
    }

    // Called by Resume button
    public void ResumeGame()
    { 
        isPaused = false;
        if (!isReseting)
        {
            ballRB.isKinematic = false;
            ballRB.velocity = ballVelocity;
        }
    }

    // Called by AI button
    public void ToggleAI()
    {
        if (isAIEnabled)
        {
            isAIEnabled = false;
            aiToggleText.text = "AI: OFF";
        }
        else
        {
            isAIEnabled = true;
            aiToggleText.text = "AI: ON";
        }
    }

    // Called by Audio button
    public void ToggleAudio()
    {
        if (isAudioEnabled)
        {
            isAudioEnabled = false;
            audioSource.volume = 0f;
            audioToggleText.text = "AUDIO: OFF";
        }
        else
        {
            isAudioEnabled = true;
            audioSource.volume = 1f;
            audioToggleText.text = "AUDIO: ON";
        }
    }

    // Called by Controls button
    public void ToggleControls()
    {
        if (areControlsEnabled)
        {
            areControlsEnabled = false;
            knob.SetActive(false);
            knobBackground.SetActive(false);
            hitButton.SetActive(false);
            controlsToggleText.text = "CONTROLS: OFF";
        }
        else
        {
            areControlsEnabled = true;
            knob.SetActive(true);
            knobBackground.SetActive(true);
            hitButton.SetActive(true);
            controlsToggleText.text = "CONTROLS: ON";
        }
    }
}
