﻿using UnityEngine;

public class Skip_Intro : MonoBehaviour
{
    // Inspector variables
    public Animator[] animationsToSkip;

    // Skip animations
    public void SkipIntro()
    {
        foreach (Animator anim in animationsToSkip)
            anim.SetBool("Skip", true);
        gameObject.SetActive(false);
    }
}
